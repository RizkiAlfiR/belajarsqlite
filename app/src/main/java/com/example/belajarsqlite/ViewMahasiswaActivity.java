package com.example.belajarsqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.TextView;

public class ViewMahasiswaActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    TextView textViewID, textViewNama, textViewJenisKelamin, textViewAlamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_mahasiswa);

        dbHelper = new DataHelper(this);
        textViewID = (TextView) findViewById(R.id.tvID);
        textViewNama = (TextView) findViewById(R.id.tvNama);
        textViewJenisKelamin = (TextView) findViewById(R.id.tvJenisKelamin);
        textViewAlamat = (TextView) findViewById(R.id.tvAlamat);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM mahasiswa WHERE NAMA = '" +
                getIntent().getStringExtra("NAMA") + "'", null);
        cursor.moveToFirst();

        if(cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            textViewID.setText(cursor.getString(0).toString());
            textViewNama.setText(cursor.getString(1).toString());
            textViewJenisKelamin.setText(cursor.getString(2).toString());
            textViewAlamat.setText(cursor.getString(3).toString());
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
