package com.example.belajarsqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UpdateMahasiswaActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    Button bSubmit;
    EditText editTextID, editTextNama, editTextJenisKelamin, editTextAlamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mahasiswa);
        dbHelper = new DataHelper(this);

        dbHelper = new DataHelper(this);
        editTextID = (EditText) findViewById(R.id.edtID);
        editTextNama = (EditText) findViewById(R.id.edtNama);
        editTextJenisKelamin = (EditText) findViewById(R.id.edtJenisKelamin);
        editTextAlamat = (EditText) findViewById(R.id.edtAlamat);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM mahasiswa WHERE NAMA = '" +
                getIntent().getStringExtra("NAMA") + "'", null);
        cursor.moveToFirst();

        if(cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            editTextID.setText(cursor.getString(0).toString());
            editTextNama.setText(cursor.getString(1).toString());
            editTextJenisKelamin.setText(cursor.getString(2).toString());
            editTextAlamat.setText(cursor.getString(3).toString());
        }

        bSubmit = (Button) findViewById(R.id.btnDaftar);

        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("UPDATE mahasiswa SET NAMA = '" +
                                editTextNama.getText().toString() + "', JENIS_KELAMIN = '" +
                                editTextJenisKelamin.getText().toString() + "', ALAMAT = '" +
                                editTextAlamat.getText().toString() + "' WHERE ID = '" +
                                editTextID.getText().toString() + "'");

                Toast.makeText(getApplicationContext(), "Update Successfull", Toast.LENGTH_SHORT).show();
                MainActivity.ma.RefreshList();
                finish();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
