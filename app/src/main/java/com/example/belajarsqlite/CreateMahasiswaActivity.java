package com.example.belajarsqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateMahasiswaActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    Button bSubmit;
    EditText editTextID, editTextNama, editTextJenisKelamin, editTextAlamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_mahasiswa);

        dbHelper = new DataHelper(this);
        editTextID = (EditText) findViewById(R.id.edtID);
        editTextNama = (EditText) findViewById(R.id.edtNama);
        editTextJenisKelamin = (EditText) findViewById(R.id.edtJenisKelamin);
        editTextAlamat = (EditText) findViewById(R.id.edtAlamat);
        bSubmit = (Button) findViewById(R.id.btnDaftar);

        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("INSERT INTO mahasiswa(ID, NAMA, JENIS_KELAMIN, ALAMAT) VALUES('" +
                        editTextID.getText().toString() + "','" +
                        editTextNama.getText().toString() + "','" +
                        editTextJenisKelamin.getText().toString() + "','" +
                        editTextAlamat.getText().toString() + "')");

                Toast.makeText(getApplicationContext(), "Create Successfull", Toast.LENGTH_SHORT).show();
                MainActivity.ma.RefreshList();
                finish();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
